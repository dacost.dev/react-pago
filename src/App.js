import Router from "./routes/Router";
import { SessionProvider } from "./contexts/SessionProvider";
import Header from "./components/Header";

function App() {
  return (
    <div className="App">
      <SessionProvider>
        <Header />
        <Router />
      </SessionProvider>
    </div>
  );
}

export default App;
