import { useState, useEffect } from "react";
import { useSession } from "../contexts/SessionProvider";

const useFetch = ({ URL, startFetching }) => {
  const { setFetchListData } = useSession();
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (startFetching) {
      setLoading(true);
      fetch(URL)
        .then((response) => {
          if (!response.ok) {
            setError(response.status);
            setLoading(false);
            throw new Error(`Error: ${response.status}`);
          }
          return response.json();
        })
        .then((data) => {
          setFetchListData(data);
          setData(data);
          setLoading(false);
        })
        .catch((error) => {
          console.error("Error en la solicitud:", error);
          setError(error);
          setLoading(false);
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return { data, error, loading };
};

export default useFetch;
