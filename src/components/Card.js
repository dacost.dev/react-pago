const styles = {
  cardContainer: {
    backgroundColor: "var(--secondary-color)",
    padding: "55px",
    maxWidth: "400px",
    height: "70%",
    borderRadius: "55px",
    zIndex: "999px",
    boxShadow: "13px 13px 0px 0px rgba(0, 0, 0, 0.5)",
  },
  titleContainer: { display: "grid", gridAutoFlow: "column" },
  title: { fontSize: "21px", fontWeight: "bold" },
  image: { width: "144px", borderRadius: "55px", padding: "8px" },
  body: {
    display: "grid",
    gridAutoFlow: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: "0px 21px 21px 21px",
    height: "100%",
    gridGap: "21px",
  },
  age: {
    display: "grid",
    gridAutoFlow: "column",
    gap: "21px",
    justifyContent: "end",
  },
};

function Card({ title, img, description, age }) {
  return (
    <div style={styles.cardContainer}>
      <div style={styles.titleContainer}>
        <div style={styles.title}>{title}</div>

        <div style={styles.age}>
          <div>{age}</div>
          <span class="material-symbols-outlined">event_available</span>
        </div>
      </div>
      <hr />
      <div style={styles.body}>
        <img style={styles.image} alt={`${title}`} src={img} />
        <div>{description}</div>
      </div>
    </div>
  );
}

export default Card;
