const styles = {
  gridContainer: {
    display: "grid",
    justifyContent: "center",
    justifyItems: "center",
    alignItems: "center",
    width: "100%",
    height: "100%",
    rowGap: "55px",
    columnGap: "21px",
    padding: "89px 0 89px 0 ",
  },
};

function Grid({ children }) {
  return (
    <div className="gridResponsive" style={styles.gridContainer}>
      {children}
    </div>
  );
}

export default Grid;
