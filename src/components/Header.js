const styles = {
  headerContainer: {
    padding: "21px",
    backgroundColor: "var(--secondary-color)",
    fontSize: "21px",
    height: "21px",
    position: "fixed",
    top: "0",
    left: "0",
    width: "100%",
    zIndex: "999px",
    boxShadow: "0px 0px 6px 0px rgba(0, 0, 0, 0.5)",
    display: "grid",
    gridAutoFlow: "column",
    justifyContent: "start",
    gap: "21px",
  },
};

function Header() {
  return (
    <div style={styles.headerContainer}>
      <span class="material-symbols-outlined">account_balance</span>
      <div>BANK LIST</div>
    </div>
  );
}

export default Header;
