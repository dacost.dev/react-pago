import { useState } from "react";
import useFetch from "../../hooks/useFetch";
import Grid from "../../components/Grid";
import Card from "../../components/Card";

function Home() {
  const [dataList] = useState(() => {
    const storedSession = localStorage.getItem("session");
    return storedSession ? JSON.parse(storedSession) : null;
  });

  const { data, error, loading } = useFetch({
    URL: `${process.env.REACT_APP_BANK_LIST_API_URL}`,
    startFetching: dataList === null,
  });

  const errorMessage = <div>Error</div>;
  const loadingMessage = <div>Loading...</div>;
  const dataDisplayList =
    !error && !loading ? (
      <Grid>
        {dataList
          ? dataList.map((x) => (
              <Card
                title={x.bankName}
                age={`${x.age} years`}
                img={x.url}
                description={x.description}
              />
            ))
          : data.map((x) => (
              <Card
                title={x.bankName}
                age={`${x.age} years`}
                img={x.url}
                description={x.description}
              />
            ))}
      </Grid>
    ) : null;

  return (
    <>
      {loading && !error ? loadingMessage : null}
      {error ? errorMessage : null}
      {data || dataList ? dataDisplayList : null}
    </>
  );
}

export default Home;
