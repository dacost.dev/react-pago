import React, { createContext, useState, useContext } from "react";

const SessionContext = createContext();

export function SessionProvider({ children }) {
  const [session, setSession] = useState(() => {
    const storedSession = localStorage.getItem("session");
    return storedSession ? JSON.parse(storedSession) : null;
  });

  const setFetchListData = (user) => {
    setSession(user);
    localStorage.setItem("session", JSON.stringify(user));
  };

  return (
    <SessionContext.Provider value={{ session, setFetchListData }}>
      {children}
    </SessionContext.Provider>
  );
}

export const useSession = () => {
  return useContext(SessionContext);
};
