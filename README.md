# Bank List Project

This project is a simple web application that displays a list of banks. It uses React library.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [License](#license)

## Getting Started

### Prerequisites

Make sure you have the following tools installed on your machine:

- Node.js (v20.11.0) LTS

### Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/dacost.dev/react-pago.git
   ```

2. Install dependencies:

   ```bash
   cd react-pago
   npm install
   ```

1. Run server:

   ```bash
   npm start
   ```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
